# {{cookiecutter.paper_title}}

[![pipeline status](https://gitlab.com/{{cookiecutter.gitlab_username}}/{{cookiecutter.paper_slug}}/badges/master/pipeline.svg)](https://gitlab.com/{{cookiecutter.gitlab_username}}/{{cookiecutter.paper_slug}}/commits/master)

{{cookiecutter.paper_short_description}}

This repository is based on [How to annoy your co-authors: a Gitlab CI pipeline for LaTeX](https://blog.martisak.se/2020/05/11/gitlab-ci-latex-pipeline/).

## Getting started

Compile locally with 

`make clean render`

or 

`make clean render LATEXMK_OPTIONS_EXTRA=-pvc` to keep compiling the PDF when the input files are updated.

Compilation is done using Docker image <code>{{cookiecutter.docker_image}}</code>.

Note that the local compilation of the `figures` directory might fail due to the Python dependencies not being installed. Fix by installing dependencies from `figure/requirements.txt` in your favorite way.

The test stage could fail due to Ruby dependencies not being installed. Install with `bundle install` before running `make check`.
