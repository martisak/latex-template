#!/bin/bash

git init

# Setup gitignore
curl https://raw.githubusercontent.com/github/gitignore/master/Global/SublimeText.gitignore > .gitignore
curl https://raw.githubusercontent.com/github/gitignore/master/TeX.gitignore >> .gitignore

# Replace fancy strings with directory name
find . -type f -iname "{{cookiecutter.paper_slug}}.*" -exec sed -i.bak "s+\$((INSTALL))+$PWD+g" "{}" +;
rm {{cookiecutter.paper_slug}}.*.bak

# If the Gitlab API private token variable is set, then create a new project.
# Then initialize git and push initial commit.

if [ ! -z "$GITLAB_API_PRIVATE_TOKEN" ]
then

    # Create Gitlab Project
    curl -H "Content-Type:application/json" https://$GITLAB_URL/api/v4/projects\?private_token\=$GITLAB_API_PRIVATE_TOKEN -d "{\"name\": \"{{cookiecutter.paper_title}}\", \"path\": \"{{cookiecutter.paper_slug}}\", \"description\": \"{{cookiecutter.paper_short_description}}\"}" | jq

    # Add download PDF badge
    curl -H "Content-Type:application/json" https://$GITLAB_URL/api/v4/projects/{{cookiecutter.gitlab_username}}%2F{{cookiecutter.paper_slug}}/badges\?private_token\=$GITLAB_API_PRIVATE_TOKEN -d "{\"link_url\": \"https://$GITLAB_URL/%{project_path}/-/jobs/artifacts/%{default_branch}/raw/{{cookiecutter.paper_slug}}.pdf?job=compile\", \"image_url\": \"https://img.shields.io/badge/Download-PDF-green\"}" | jq

    # Add pipeline status badge
    curl -H "Content-Type:application/json" https://$GITLAB_URL/api/v4/projects/{{cookiecutter.gitlab_username}}%2F{{cookiecutter.paper_slug}}/badges\?private_token\=$GITLAB_API_PRIVATE_TOKEN -d "{\"link_url\": \"https://$GITLAB_URL/%{project_path}/commits/%{default_branch}\", \"image_url\": \"https://$GITLAB_URL/%{project_path}/badges/master/pipeline.svg\"}" | jq

    # Add made with LaTeX badge
    curl -H "Content-Type:application/json" https://$GITLAB_URL/api/v4/projects/{{cookiecutter.gitlab_username}}%2F{{cookiecutter.paper_slug}}/badges\?private_token\=$GITLAB_API_PRIVATE_TOKEN -d "{\"link_url\": \"https://www.latex-project.org/\", \"image_url\": \"https://img.shields.io/badge/Made%20with-LaTeX-1f425f.svg\"}" | jq


    git remote add origin git@$GITLAB_URL:{{cookiecutter.gitlab_username}}/{{cookiecutter.paper_slug}}.git

fi

git add .
git commit -m "Initial commit"

# If we have a remote repository, then let's push.
if [ ! -z "$GITLAB_API_PRIVATE_TOKEN" ]
then
    git push -u origin master
fi